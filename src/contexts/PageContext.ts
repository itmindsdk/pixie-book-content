import { createContext } from "react";

import Area from "../content-schema/Area";
import { findDataInTreeByIds } from "../utils/findDataInTreeByIndex";

export const PageContext = createContext<{
  findDataByIds: ReturnType<typeof findDataInTreeByIds>;
  data: Area[];
}>(null);
