import { FC } from "react";

import Area from "../../content-schema/Area";
import styles from "./Content.module.css";

type Props = {
  area: Partial<Area>;
};

const AreaComponent: FC<Props> = ({ area }) => {
  return (
    <div>
      <h1 className={styles.title} style={{ color: area.color }}>
        {area.title}
      </h1>
      <p>{area.description}</p>
    </div>
  );
};

export default AreaComponent;
