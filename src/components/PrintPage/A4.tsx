import { FC } from "react";

import styles from "./A4.module.css";

type Props = {
  pageNum: number;
  color?: string;
  colorName?: string;
};

const A4Page: FC<Props> = ({ children, pageNum, color = "#eee", colorName = "" }) => {
  return (
    <div className={styles.page}>
      <div className={styles.frame}>
        <div className={styles.bleedBoxTopRight} />
        <div className={styles.bleedBoxTopLeft} />
        <div className={styles.bleedBoxBottomRight} />
        <div className={styles.bleedBoxBottomLeft} />

        {pageNum > 0 && (
          <>
            <div
              className={pageNum % 2 === 0 ? styles.bannerRight : styles.bannerLeft}
              style={{ background: color }}>
              {colorName}
            </div>

            <div className={pageNum % 2 === 0 ? styles.pageNumRight : styles.pageNumLeft}>
              {pageNum}
            </div>
          </>
        )}

        <div className={styles.content}>{children}</div>
      </div>
    </div>
  );
};

export default A4Page;
