import { FC } from "react";

import Area from "../../content-schema/Area";
import SubArea from "../../content-schema/SubArea";
import styles from "./Content.module.css";

type Props = {
  subarea: Partial<SubArea>;
  area: Partial<Area>;
};

const SubAreaComponent: FC<Props> = ({ subarea, area }) => {
  return (
    <div>
      <h1 className={styles.title} style={{ color: area.color }}>
        {subarea.title}
      </h1>
      <p>{subarea.description}</p>
    </div>
  );
};

export default SubAreaComponent;
