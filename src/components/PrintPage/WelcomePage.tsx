import { FC } from "react";

import styles from "./Content.module.css";

type Props = {
  // area: Area;
};

const WelcomePage: FC<Props> = () => {
  return (
    <div>
      <h1 className={styles.title}>Tech i IT Minds.</h1>
      <p>
        Dette er din personlige håndbog til at forstå hvad udvikling og den store verden der følger
        er.
      </p>
      <p>
        <b>Denne bog tilhører ___________</b>
      </p>
    </div>
  );
};

export default WelcomePage;
