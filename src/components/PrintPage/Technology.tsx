import Link from "next/link";
import { FC, useContext } from "react";

import Area from "../../content-schema/Area";
import SubArea from "../../content-schema/SubArea";
import Technology from "../../content-schema/Technology";
import { PageContext } from "../../contexts/PageContext";
import CodeButton from "../CodeButton/CodeButton";
import styles from "./Content.module.css";

type Props = {
  tech: Partial<Technology>;
  subarea: Partial<SubArea>;
  area: Partial<Area>;
};

const TechComponent: FC<Props> = ({ tech, area }) => {
  const { findDataByIds } = useContext(PageContext);

  return (
    <div>
      <h1 className={styles.title} style={{ color: area?.color }}>
        {tech.title}
      </h1>
      <p className={styles.byline}>{tech.subTitle}</p>
      <p>{tech.text}</p>
      <div className={styles.infoBoxes}>
        <div className={styles.infoBox}>
          <i>Lign. Teknologier</i>:
          <div className={styles.tags}>
            {tech.relatedTechnologies.map((t, i) => {
              const found = findDataByIds(t);
              return found ? (
                <Link key={found.id} href={found.fullLink} passHref>
                  <CodeButton found={found} tabIndex={i} />
                </Link>
              ) : (
                <code key={"tech" + t + i} className={styles.code}>
                  {t}
                </code>
              );
            })}
          </div>
        </div>
        <div className={styles.infoBoxRight}>
          <i>Keywords</i>:
          <div className={styles.tags}>
            {tech.keywords.map((t, i) => {
              const found = findDataByIds(t);
              return found ? (
                <Link
                  key={found.id}
                  href={{
                    pathname: found.fullLinkTemplate,
                    query: {
                      area: (found as any).areaId ?? found.id,
                      subarea: (found as any).subAreaId ?? found.id,
                      tech: found.id
                    }
                  }}
                  passHref>
                  <CodeButton found={found} tabIndex={i} />
                </Link>
              ) : (
                <code key={"key" + t + i} className={styles.code}>
                  {t}
                </code>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
};

export default TechComponent;
