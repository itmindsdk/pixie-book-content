import { forwardRef } from "react";

import { Content } from "../../content-schema/Content";
import styles from "../PrintPage/Content.module.css";

type Props = {
  tabIndex: number;
  found: Partial<Content>;
  // following types are from Next/Link passRef
  onClick?;
  onMouseEnter?;
  href?;
};
const CodeButton = forwardRef<HTMLElement, Props>(
  ({ onClick, href, onMouseEnter, tabIndex, found }, ref) => {
    return (
      <code
        ref={ref}
        className={styles.codeclick}
        tabIndex={tabIndex}
        role="button"
        onMouseEnter={onMouseEnter}
        onKeyPress={onClick}
        onClick={onClick}
        {...{ href }}>
        {found.title}, s{found.index}
      </code>
    );
  }
);
CodeButton.displayName = "CodeButton";

export default CodeButton;
