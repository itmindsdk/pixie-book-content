import { IsNotEmpty, MaxLength, MinLength } from "class-validator";
import { JSONSchema, validationMetadatasToSchemas } from "class-validator-jsonschema";

import { Content } from "./Content";
import Technology from "./Technology";

@JSONSchema({
  description: "",
  example: {}
})
export default class SubArea implements Content {
  @IsNotEmpty()
  @MinLength(1)
  id: string;

  @MinLength(1)
  areaId: string;

  @IsNotEmpty()
  @MinLength(1)
  @MaxLength(100)
  title: string;

  @IsNotEmpty()
  @MinLength(1)
  @MaxLength(2000)
  description: string;

  techs: Partial<Technology>[] = [];
  index: number;
  path: string;

  get fullLink(): string {
    return `/${this.areaId}/${this.id}`;
  }

  fullLinkTemplate = "/[area]/[subarea]";

  constructor(obj: Partial<SubArea>, index?: number, path?: string) {
    this.index = index;
    this.path = path;
    Object.assign(this, obj);
  }
}

export const genSchema = () => validationMetadatasToSchemas();
