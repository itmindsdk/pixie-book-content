import { IsNotEmpty, Matches, MaxLength, MinLength } from "class-validator";
import { JSONSchema, validationMetadatasToSchemas } from "class-validator-jsonschema";

import { Content } from "./Content";
import SubArea from "./SubArea";

@JSONSchema({
  description: "",
  example: {}
})
export default class Area implements Content {
  @IsNotEmpty()
  @MinLength(1)
  id: string;

  @IsNotEmpty()
  @MinLength(1)
  @MaxLength(100)
  title: string;

  @IsNotEmpty()
  @Matches(/^#[0-9a-fA-F]{3,6}$/)
  color: string;

  @IsNotEmpty()
  @MinLength(1)
  @MaxLength(2000)
  description: string;

  subAreas: Partial<SubArea>[] = [];
  index: number;
  path: string;

  get fullLink(): string {
    return "/" + this.id;
  }

  fullLinkTemplate = "/[area]";

  constructor(obj: Partial<Area>, index?: number, path?: string) {
    this.index = index;
    this.path = path;
    Object.assign(this, obj);
  }
}

export const genSchema = () => validationMetadatasToSchemas();
