export interface Content {
  $schema?: string;
  index: number;
  id: string;
  title: string;
  path: string;
  fullLink: string;
  fullLinkTemplate: string;
}
