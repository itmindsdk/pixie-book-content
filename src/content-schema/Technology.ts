import {
  ArrayMaxSize,
  IsArray,
  IsNotEmpty,
  IsOptional,
  IsString,
  MaxLength,
  MinLength
} from "class-validator";
import { JSONSchema, validationMetadatasToSchemas } from "class-validator-jsonschema";

import { Content } from "./Content";

@JSONSchema({
  description: "",
  example: {}
})
export default class Technology implements Content {
  @IsNotEmpty()
  @MinLength(1)
  id: string;

  @IsNotEmpty()
  @MinLength(1)
  areaId: string;

  @IsNotEmpty()
  @MinLength(1)
  subAreaId: string;

  @IsNotEmpty()
  @MinLength(1)
  @MaxLength(100)
  title: string;

  @IsNotEmpty()
  @MinLength(1)
  @MaxLength(5000)
  text: string;

  @IsOptional()
  @MinLength(1)
  @MaxLength(100)
  subTitle;

  @ArrayMaxSize(9)
  @IsArray()
  @IsString({ each: true })
  relatedTechnologies: string[];

  @ArrayMaxSize(9)
  @IsArray()
  @IsString({ each: true })
  keywords: string[];

  index: number;
  path: string;

  get fullLink(): string {
    return `/${this.areaId}/${this.subAreaId}/${this.id}`;
  }

  fullLinkTemplate = "/[area]/[subarea]/[tech]";

  constructor(obj: Partial<Technology>, index?: number, path?: string) {
    this.index = index;
    this.path = path;
    Object.assign(this, obj);
  }
}

export const genSchema = () => validationMetadatasToSchemas();
