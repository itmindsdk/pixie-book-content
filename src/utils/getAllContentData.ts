import { readdirSync, readFileSync } from "fs";
import { join } from "path";

import Area from "../content-schema/Area";
import SubArea from "../content-schema/SubArea";
import Technology from "../content-schema/Technology";

// const contentPath = resolve(__dirname, "..", "..", "content");
const jsonPattern = /^([\w_-]+)\.json$/;
const dirPattern = /^[\w_-]+$/;
const indexPattern = /^index.json$/;

let cache: [Area[], SubArea[], Technology[]] = null;

export const getAllContentData = (contentPath: string): typeof cache => {
  if (cache !== null) {
    console.log("Using getAllContentData cache");
    return cache;
  }

  const areas: Area[] = [];
  const subareas: SubArea[] = [];
  const techs: Technology[] = [];

  let index = 1;

  readdirSync(contentPath).map(areaPath => {
    if (!dirPattern.test(areaPath)) return;

    const currentPath = join(contentPath, areaPath);

    readdirSync(currentPath).map(areaContent => {
      const fullAreaPath = join(currentPath, areaContent);

      if (indexPattern.test(areaContent)) {
        const area = JSON.parse(readFileSync(fullAreaPath).toString());
        areas.push(new Area(area, index++, areaPath));
        return;
      }

      if (!dirPattern.test(areaContent)) return;

      readdirSync(fullAreaPath)
        .sort((a, b) => {
          if (indexPattern.test(a)) return -1;
          if (indexPattern.test(b)) return 1;
          return 0;
        })
        .map(subAreaContent => {
          const subAreaPath = join(fullAreaPath, subAreaContent);

          console.debug("CHECK INDEX", subAreaPath, index);

          if (indexPattern.test(subAreaContent)) {
            const subArea = JSON.parse(readFileSync(subAreaPath).toString());

            subareas.push(new SubArea(subArea, index++, areaContent));

            return;
          }

          if (!jsonPattern.test(subAreaContent)) return;

          const technology = JSON.parse(readFileSync(subAreaPath).toString());
          const techPath = jsonPattern.exec(subAreaContent)[1];

          techs.push(new Technology(technology, index++, techPath));
        });
    });
  });
  cache = [areas, subareas, techs];
  return cache;
};
