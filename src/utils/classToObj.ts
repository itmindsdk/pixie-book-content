export const classToObj = (instance = {}): unknown => {
  const keys = Object.keys(instance);
  return keys.reduce((classAsObj, key) => {
    if (instance[key] === undefined) {
      classAsObj[key] = null;
    } else {
      classAsObj[key] = instance[key];
    }
    return classAsObj;
  }, {});
};
