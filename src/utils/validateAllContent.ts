import { validateOrReject } from "class-validator";
import { resolve } from "path";

import { Content } from "../content-schema/Content";
import { getAllContentData } from "./getAllContentData";

const contentPath = resolve(__dirname, "..", "..", "content");

let errors = 0;

const validate = async (content: Content) => {
  try {
    if (content.$schema) delete content.$schema;
    await validateOrReject(content, {
      whitelist: true
    });
    console.info("Validation complete: [OK]");
  } catch (err) {
    console.error("Validation complete: [ERROR]");
    console.debug(JSON.stringify(err, null, 2));
    errors++;
  }
};

const allData = getAllContentData(contentPath);

const [areas, subareas, techs] = allData;

const allIdsAreUnique = allData
  .flatMap((x: Content[]) => x.map(x => x.id))
  .every((x, index, self) => self.indexOf(x) === index);

if (!allIdsAreUnique) {
  console.error("Some schemas do not have unique ID");
  process.exit(1);
}

Promise.all([
  ...areas.map(x => validate(x)),
  ...subareas.map(x => validate(x)),
  ...techs.map(x => validate(x))
])
  .then(() => {
    if (errors > 0) {
      console.error("Some schemas aren't valid, check above for errors! ", errors);
      process.exit(1);
    } else {
      console.info("All schemas are valid!");
      process.exit(0);
    }
  })
  .catch(err => {
    console.error("Something went wrong during pre validation of a schema");
    console.debug(JSON.stringify(err, null, 2));
    process.exit(1);
  });
