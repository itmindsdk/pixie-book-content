import Area from "../content-schema/Area";
import SubArea from "../content-schema/SubArea";
import Technology from "../content-schema/Technology";
import { getAllContentData } from "./getAllContentData";

export const getAllContentDataTree = ([areas, subareas, techs]: ReturnType<
  typeof getAllContentData
>): Area[] => {
  return areas.map(a => {
    const area = new Area(a);
    const matchedSubAreas = subareas.filter(s => s.areaId === area.id);
    area.subAreas = matchedSubAreas.map(s => {
      const subarea = new SubArea(s);
      const matchedTechs = techs.filter(t => t.subAreaId === subarea.id && t.areaId === area.id);
      subarea.techs = matchedTechs.map(t => new Technology(t));
      return subarea;
    });
    return area;
  });
};
