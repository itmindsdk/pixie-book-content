import "../content-schema/Area";
import "../content-schema/SubArea";

import { writeFileSync } from "fs";
import { resolve } from "path";

import { genSchema as genTechSchema } from "../content-schema/Technology";

const definitions = {
  $schema: "http://json-schema.org/draft-07/schema#",
  $id: "https://bitbucket.org/itmindsdk/pixie-book-content/raw/main/schemas/definitions.json",
  definitions: genTechSchema()
};

const technologySchema = {
  $schema: "http://json-schema.org/draft-07/schema#",
  $id: "https://bitbucket.org/itmindsdk/pixie-book-content/raw/main/schemas/technology-schema.json",
  $ref: "definitions.json#/definitions/Technology",
  additionalProperties: false
};

const areaSchema = {
  $schema: "http://json-schema.org/draft-07/schema#",
  $id: "https://bitbucket.org/itmindsdk/pixie-book-content/raw/main/schemas/area-schema.json",
  $ref: "definitions.json#/definitions/Area",
  additionalProperties: false
};

const subAreaSchema = {
  $schema: "http://json-schema.org/draft-07/schema#",
  $id: "https://bitbucket.org/itmindsdk/pixie-book-content/raw/main/schemas/sub-area-schema.json",
  $ref: "definitions.json#/definitions/SubArea",
  additionalProperties: false
};

writeFileSync(
  resolve(__dirname, "..", "..", "schemas", "definitions.json"),
  JSON.stringify(definitions, null, 2) + "\n"
);

writeFileSync(
  resolve(__dirname, "..", "..", "schemas", "technology-schema.json"),
  JSON.stringify(technologySchema, null, 2) + "\n"
);

writeFileSync(
  resolve(__dirname, "..", "..", "schemas", "area-schema.json"),
  JSON.stringify(areaSchema, null, 2) + "\n"
);

writeFileSync(
  resolve(__dirname, "..", "..", "schemas", "sub-area-schema.json"),
  JSON.stringify(subAreaSchema, null, 2) + "\n"
);

export {};
