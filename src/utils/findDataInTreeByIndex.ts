import { Content } from "../content-schema/Content";
import { getAllContentDataTree } from "./getAllContentDataTree";

export const findDataInTreeByIndex = (data: ReturnType<typeof getAllContentDataTree>) => (
  index: number
): Partial<Content> => {
  const dataCopy = [...data];

  const area = dataCopy.reverse().find(a => a.index <= index);

  if (area.index === index) return area;

  const subarea = area.subAreas.reverse().find(s => s.index <= index);

  if (subarea.index === index) return subarea;

  const tech = subarea.techs.find(s => s.index === index);
  return tech;
};

export const findDataInTreeByIds = (data: ReturnType<typeof getAllContentDataTree>) => (
  searchText: string
): Partial<Content> => {
  let foundContent: Partial<Content> = null;

  if (!data) return null;

  data.some(a => {
    if (a.id === searchText) {
      foundContent = a;
      return true;
    }
    return false;
  });
  if (foundContent !== null) return foundContent;

  data.some(a =>
    a.subAreas.some(s => {
      if (s.id === searchText) {
        foundContent = s;
        return true;
      }
      return false;
    })
  );
  if (foundContent !== null) return foundContent;

  data.some(a =>
    a.subAreas.some(s =>
      s.techs.some(t => {
        if (t.id === searchText) {
          foundContent = t;
          return true;
        }
        return false;
      })
    )
  );
  return foundContent;
};
