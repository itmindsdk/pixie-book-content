import "./global.css";

import { AppProps } from "next/dist/next-server/lib/router/router";
import { FC } from "react";

import { getAllContentData } from "../utils/getAllContentData";

type Props = {
  data: ReturnType<typeof getAllContentData>;
};

const MyApp: FC<AppProps & Props> = ({ Component, pageProps }) => {
  return <Component {...pageProps} />;
};

export default MyApp;
