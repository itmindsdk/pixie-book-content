import { GetStaticProps, NextPage } from "next";
import { resolve } from "path";
import { createContext, useMemo } from "react";

import A4Page from "../components/PrintPage/A4";
import AreaComponent from "../components/PrintPage/Area";
import SubAreaComponent from "../components/PrintPage/SubArea";
import TechComponent from "../components/PrintPage/Technology";
import WelcomePage from "../components/PrintPage/WelcomePage";
import { Content } from "../content-schema/Content";
import { PageContext } from "../contexts/PageContext";
import { classToObj } from "../utils/classToObj";
import { findDataInTreeByIds } from "../utils/findDataInTreeByIndex";
import { getAllContentData } from "../utils/getAllContentData";
import { getAllContentDataTree } from "../utils/getAllContentDataTree";

type Props = {
  data: ReturnType<typeof getAllContentData>;
};

const IndexPage: NextPage<Props> = ({ data: initialData }) => {
  const data = useMemo(() => getAllContentDataTree(initialData), []);
  const findDataByIds = useMemo(() => findDataInTreeByIds(data), [data]);

  return (
    <PageContext.Provider value={{ findDataByIds, data }}>
      <A4Page pageNum={0}>
        <WelcomePage />
      </A4Page>
      {data.map(a => (
        <div key={a.id}>
          <A4Page pageNum={a.index} color={a.color}>
            <AreaComponent area={a} />
          </A4Page>

          {a.subAreas.map(s => (
            <div key={a.id + s.id}>
              <A4Page pageNum={s.index} color={a.color} colorName={s.title}>
                <SubAreaComponent subarea={s} area={a} />
              </A4Page>
              {s.techs.map(t => (
                <div key={a.id + s.id + t.id}>
                  <A4Page pageNum={t.index} color={a.color} colorName={s.title}>
                    <TechComponent tech={t} subarea={s} area={a} />
                  </A4Page>
                </div>
              ))}
            </div>
          ))}
        </div>
      ))}
    </PageContext.Provider>
  );
};

export const getStaticProps: GetStaticProps<Props> = async () => {
  const path = resolve(process.cwd(), "content");
  const allContent = getAllContentData(path);

  return {
    props: {
      data: allContent.map((x: Content[]) => x.map(y => classToObj(y))) as typeof allContent
    }
  };
};

export default IndexPage;
