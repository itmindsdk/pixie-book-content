import { GetStaticPaths, GetStaticProps, NextPage } from "next";
import Link from "next/link";
import { resolve } from "path";
import { useMemo } from "react";

import AreaComponent from "../../components/PrintPage/Area";
import { Content } from "../../content-schema/Content";
import { classToObj } from "../../utils/classToObj";
import { getAllContentData } from "../../utils/getAllContentData";
import { getAllContentDataTree } from "../../utils/getAllContentDataTree";

type Props = {
  data: ReturnType<typeof getAllContentData>;
  id: string;
};

const AreaPage: NextPage<Props> = ({ data: initialData, id }) => {
  const data = useMemo(() => getAllContentDataTree(initialData), []);

  const model = useMemo(() => data.find(x => x.id === id), [data]);

  return (
    <div>
      <AreaComponent area={model} />
      {model.subAreas.map(link => (
        <Link key={link.id} href={`/${model.path}/${link.id}`}>
          <a>
            -&gt; {link.title}
            <br />
          </a>
        </Link>
      ))}
    </div>
  );
};

export const getStaticProps: GetStaticProps<Props> = async context => {
  const { area: id } = context.params;

  // NOTE: https://nextjs.org/docs/basic-features/data-fetching#reading-files-use-processcwd
  const path = resolve(process.cwd(), "content");
  const allContent = getAllContentData(path);

  return {
    props: {
      data: allContent.map((x: Content[]) => x.map(y => classToObj(y))) as typeof allContent,
      id: id as string,
      key: id as string
    }
  };
};

// NOTE: https://nextjs.org/docs/basic-features/data-fetching#getstaticpaths-static-generation
export const getStaticPaths: GetStaticPaths = async () => {
  const path = resolve(process.cwd(), "content");
  const [areas] = getAllContentData(path);

  const paths = areas.map(x => ({ params: { area: x.id } }));

  return {
    paths: paths,
    fallback: false
  };
};

export default AreaPage;
