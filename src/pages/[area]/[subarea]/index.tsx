import { GetStaticPaths, GetStaticProps, NextPage } from "next";
import Link from "next/link";
import { resolve } from "path";
import { useMemo } from "react";

import SubAreaComponent from "../../../components/PrintPage/SubArea";
import { Content } from "../../../content-schema/Content";
import { classToObj } from "../../../utils/classToObj";
import { getAllContentData } from "../../../utils/getAllContentData";
import { getAllContentDataTree } from "../../../utils/getAllContentDataTree";

type Props = {
  data: ReturnType<typeof getAllContentData>;
  areaId: string;
  id: string;
};

const SubAreaPage: NextPage<Props> = ({ areaId, id, data: initialData }) => {
  const data = useMemo(() => getAllContentDataTree(initialData), []);
  const area = useMemo(() => data.find(x => x.id === areaId), [data]);
  const model = useMemo(() => area.subAreas.find(x => x.id === id), [data, area]);

  return (
    <div>
      <SubAreaComponent area={area} subarea={model} />
      {model.techs.map(link => (
        <Link key={link.id} href={link.fullLink}>
          <a>
            --&gt; {link.title}
            <br />
          </a>
        </Link>
      ))}
    </div>
  );
};

export const getStaticProps: GetStaticProps<Props> = async context => {
  const { subarea: id, area: areaId } = context.params;

  // NOTE: https://nextjs.org/docs/basic-features/data-fetching#reading-files-use-processcwd
  const path = resolve(process.cwd(), "content");
  const allContent = getAllContentData(path);

  return {
    props: {
      data: allContent.map((x: Content[]) => x.map(y => classToObj(y))) as typeof allContent,
      id: id as string,
      areaId: areaId as string,
      key: id as string
    }
  };
};

// NOTE: https://nextjs.org/docs/basic-features/data-fetching#getstaticpaths-static-generation
export const getStaticPaths: GetStaticPaths = async () => {
  const path = resolve(process.cwd(), "content");
  const areas = getAllContentDataTree(getAllContentData(path));

  const paths = areas.flatMap(area =>
    area.subAreas.map(subarea => ({
      params: { area: area.id, subarea: subarea.id }
    }))
  );

  return {
    paths: paths,
    fallback: false
  };
};

export default SubAreaPage;
