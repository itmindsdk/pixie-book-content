import { GetStaticPaths, GetStaticProps, NextPage } from "next";
import { resolve } from "path";
import { useMemo } from "react";

import TechnologyPage from "../../../components/PrintPage/Technology";
import { Content } from "../../../content-schema/Content";
import { PageContext } from "../../../contexts/PageContext";
import { classToObj } from "../../../utils/classToObj";
import { findDataInTreeByIds } from "../../../utils/findDataInTreeByIndex";
import { getAllContentData } from "../../../utils/getAllContentData";
import { getAllContentDataTree } from "../../../utils/getAllContentDataTree";

type Props = {
  data: ReturnType<typeof getAllContentData>;
  areaId: string;
  subareaId: string;
  id: string;
};

const TechPage: NextPage<Props> = ({ subareaId, areaId, id, data: initialData }) => {
  const data = useMemo(() => getAllContentDataTree(initialData), []);
  const area = useMemo(() => data.find(x => x.id === areaId), [data]);
  const subarea = useMemo(() => area.subAreas.find(x => x.id === subareaId), [data, area]);
  const model = useMemo(() => subarea.techs.find(x => x.id === id), [data, area, subarea]);

  const findDataByIds = useMemo(() => findDataInTreeByIds(data), [data]);

  return (
    <PageContext.Provider value={{ findDataByIds, data }}>
      <TechnologyPage area={area} subarea={subarea} tech={model} />
    </PageContext.Provider>
  );
};

export const getStaticProps: GetStaticProps<Props> = async context => {
  const { tech: id, subarea: subareaId, area: areaId } = context.params;

  // NOTE: https://nextjs.org/docs/basic-features/data-fetching#reading-files-use-processcwd
  const path = resolve(process.cwd(), "content");
  const allContent = getAllContentData(path);

  return {
    props: {
      data: allContent.map((x: Content[]) => x.map(y => classToObj(y))) as typeof allContent,
      id: id as string,
      areaId: areaId as string,
      subareaId: subareaId as string,
      key: id as string
    }
  };
};

// NOTE: https://nextjs.org/docs/basic-features/data-fetching#getstaticpaths-static-generation
export const getStaticPaths: GetStaticPaths = async () => {
  const path = resolve(process.cwd(), "content");

  const areas = getAllContentDataTree(getAllContentData(path));

  const paths = areas.flatMap(area =>
    area.subAreas.flatMap(subarea =>
      subarea.techs.map(tech => ({
        params: { area: area.id, subarea: subarea.id, tech: tech.id }
      }))
    )
  );

  return {
    paths: paths,
    fallback: false
  };
};

export default TechPage;
